#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "rsa.h"
#include "prf.h"

/* NOTE: a random composite surviving 10 Miller-Rabin tests is extremely
 * unlikely.  See Pomerance et al.:
 * http://www.ams.org/mcom/1993-61-203/S0025-5718-1993-1189518-9/
 * */
#define ISPRIME(x) mpz_probab_prime_p(x,10)
#define NEWZ(x) mpz_t x; mpz_init(x)
#define BYTES2Z(x,buf,len) mpz_import(x,len,-1,1,0,0,buf)
#define Z2BYTES(buf,len,x) mpz_export(buf,&len,-1,1,0,0,x)

/* utility function for read/write mpz_t with streams: */
int zToFile(FILE* f, mpz_t x)
{
	size_t i,len = mpz_size(x)*sizeof(mp_limb_t);
	/* NOTE: len may overestimate the number of bytes actually required. */
	unsigned char* buf = malloc(len);
	Z2BYTES(buf,len,x);
	/* force little endian-ness: */
	for (i = 0; i < 8; i++) {
		unsigned char b = (len >> 8*i) % 256;
		fwrite(&b,1,1,f);
	}
	fwrite(buf,1,len,f);
	/* kill copy in buffer, in case this was sensitive: */
	memset(buf,0,len);
	free(buf);
	return 0;
}
int zFromFile(FILE* f, mpz_t x)
{
	size_t i,len=0;
	/* force little endian-ness: */
	for (i = 0; i < 8; i++) {
		unsigned char b;
		/* XXX error check this; return meaningful value. */
		fread(&b,1,1,f);
		len += (b << 8*i);
	}
	unsigned char* buf = malloc(len);
	fread(buf,1,len,f);
	BYTES2Z(x,buf,len);
	/* kill copy in buffer, in case this was sensitive: */
	memset(buf,0,len);
	free(buf);
	return 0;
}


// Generate a random prime numbers
void generate_random_prime(mpz_t prime_num,unsigned char* buf,size_t bytes){
	randBytes(buf,bytes);
	BYTES2Z(prime_num,buf,bytes);
	while(!ISPRIME(prime_num)){
		randBytes(buf,bytes);
		BYTES2Z(prime_num,buf,bytes);
	}
}

int rsa_keyGen(size_t keyBits, RSA_KEY* K)
{
	rsa_initKey(K);
	/* TODO: write this.  Use the prf to get random byte strings of
	 * the right length, and then test for primality (see the ISPRIME
	 * macro above).  Once you've found the primes, set up the other
	 * pieces of the key ({en,de}crypting exponents, and n=pq). */
	size_t key_bytes = keyBits / 8;
	unsigned char* buf = (unsigned char*)malloc(key_bytes);

	// Generate random prime numebers for p and q
	generate_random_prime(K->p,buf,key_bytes);
	generate_random_prime(K->q,buf,key_bytes);

	// Pointer to the internals of the GMP integer data structure
	mpz_t p_minus_one;
	mpz_t q_minus_one;
	mpz_t phi;
	mpz_t n;
	// Allocate Memory 
	mpz_init(p_minus_one);
	mpz_init(q_minus_one);
	mpz_init(phi);

	// Set n where n=p*q
	mpz_mul(n,K->p,K->q);
	mpz_set(K->n,n);

	// Find the number of co prime numbers of n
	// PHI(n) = (p - 1)(q - 1)
	mpz_sub_ui(p_minus_one,K->p,1);
	mpz_sub_ui(q_minus_one,K->q,1);
	mpz_mul(phi,p_minus_one,q_minus_one);	

	// Choose e = {1 < e < PHI(n) and coprime with n & PHI(n)}
	gmp_randstate_t state;
	gmp_randinit_mt(state);
	mpz_urandomm(K->e,state,phi);
	mpz_t gcd_n;
	mpz_t gcd_phi;
	mpz_init(gcd_n);
	mpz_init(gcd_phi);
	mpz_gcd(gcd_phi,K->e,phi);
	mpz_gcd(gcd_n,K->e,n);

	while((mpz_cmp_ui(gcd_phi,1) != 0 || mpz_cmp_ui(gcd_n,1) != 0) || mpz_cmp_ui(K->e,1) == 0){
		mpz_urandomm(K->e,state,phi);
		mpz_gcd(gcd_phi,K->e,phi);
		mpz_gcd(gcd_n,K->e,n);
	}

	// Choose d such that e*d (mod phi(n)) = 1
	mpz_invert(K->d, K->e , phi);

	free(buf);
	mpz_clear(p_minus_one);
	mpz_clear(q_minus_one);
	mpz_clear(phi);
	mpz_clear(n);
	mpz_clear(gcd_n);
	mpz_clear(gcd_phi);
	gmp_randclear(state);
	return 0;
}

size_t rsa_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		RSA_KEY* K)
{
	/* TODO: write this.  Use BYTES2Z to get integers, and then
	 * Z2BYTES to write the output buffer. */

	// Pointer to the internals of the GMP integer data structure
	mpz_t encrypted;

	// Allocate Memory 
	mpz_init(encrypted);

	// Grap the message from buffer and convert from bytes to mpz_t
	BYTES2Z(encrypted,inBuf,len);

	// Encrypt Message (message^e mod (n))
	mpz_powm_sec(encrypted,encrypted,K->e,K->n);

	// Convert mpz_t to bytes and Sent the encrypted message to the buffer
	Z2BYTES(outBuf, len, encrypted);

	mpz_clear(encrypted);

	return len; /* TODO: return should be # bytes written */
}
size_t rsa_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		RSA_KEY* K)
{
		/* TODO: write this.  See remarks above. */

	// Pointer to the internals of the GMP integer data structure
	mpz_t decrypted;

	// Allocate Memory 
	mpz_init(decrypted);

	// Grap the message from buffer and convert message from bytes to mpz_t
	BYTES2Z(decrypted,inBuf,len);

	// decrypt Message (message^d mod (n))
	mpz_powm_sec(decrypted,decrypted,K->d,K->n);

	// Convert mpz_t to bytes and Sent the decrypted message to the buffer
	Z2BYTES(outBuf, len, decrypted);

	mpz_clear(decrypted);

	return len; 
}

size_t rsa_numBytesN(RSA_KEY* K)
{
	return mpz_size(K->n) * sizeof(mp_limb_t);
}

int rsa_initKey(RSA_KEY* K)
{
	mpz_init(K->d); mpz_set_ui(K->d,0);
	mpz_init(K->e); mpz_set_ui(K->e,0);
	mpz_init(K->p); mpz_set_ui(K->p,0);
	mpz_init(K->q); mpz_set_ui(K->q,0);
	mpz_init(K->n); mpz_set_ui(K->n,0);
	return 0;
}

int rsa_writePublic(FILE* f, RSA_KEY* K)
{
	/* only write n,e */
	zToFile(f,K->n);
	zToFile(f,K->e);
	return 0;
}
int rsa_writePrivate(FILE* f, RSA_KEY* K)
{
	zToFile(f,K->n);
	zToFile(f,K->e);
	zToFile(f,K->p);
	zToFile(f,K->q);
	zToFile(f,K->d);
	return 0;
}
int rsa_readPublic(FILE* f, RSA_KEY* K)
{
	rsa_initKey(K); /* will set all unused members to 0 */
	zFromFile(f,K->n);
	zFromFile(f,K->e);
	return 0;
}
int rsa_readPrivate(FILE* f, RSA_KEY* K)
{
	rsa_initKey(K);
	zFromFile(f,K->n);
	zFromFile(f,K->e);
	zFromFile(f,K->p);
	zFromFile(f,K->q);
	zFromFile(f,K->d);
	return 0;
}
int rsa_shredKey(RSA_KEY* K)
{
	/* clear memory for key. */
	mpz_t* L[5] = {&K->d,&K->e,&K->n,&K->p,&K->q};
	size_t i;
	for (i = 0; i < 5; i++) {
		size_t nLimbs = mpz_size(*L[i]);
		if (nLimbs) {
			memset(mpz_limbs_write(*L[i],nLimbs),0,nLimbs*sizeof(mp_limb_t));
			mpz_clear(*L[i]);
		}
	}
	/* NOTE: a quick look at the gmp source reveals that the return of
	 * mpz_limbs_write is only different than the existing limbs when
	 * the number requested is larger than the allocation (which is
	 * of course larger than mpz_size(X)) */
	return 0;
}

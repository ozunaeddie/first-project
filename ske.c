#include "ske.h"
#include "prf.h"
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <openssl/err.h>
#include <openssl/aes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memcpy */
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#ifdef LINUX
#define MMAP_SEQ MAP_PRIVATE|MAP_POPULATE
#else
#define MMAP_SEQ MAP_PRIVATE
#endif

/* NOTE: since we use counter mode, we don't need padding, as the
 * ciphertext length will be the same as that of the plaintext.
 * Here's the message format we'll use for the ciphertext:
 * +------------+--------------------+----------------------------------+
 * | 16 byte IV | C = AES(plaintext) | HMAC(IV|C) (32 bytes for SHA256) |
 * +------------+--------------------+----------------------------------+
 * */

/* we'll use hmac with sha256, which produces 32 byte output */

#define HM_LEN 32
#define KDF_KEY "qVHqkOVJLb7EolR9dsAMVwH1hRCYVx#I"
/* need to make sure KDF is orthogonal to other hash functions, like
 * the one used in the KDF, so we use hmac with a key. */

int ske_keyGen(SKE_KEY* K, unsigned char* entropy, size_t entLen)
{
	/* TODO: write this.  If entropy is given, apply a KDF to it to get
	 * the keys (something like HMAC-SHA512 with KDF_KEY will work).
	 * If entropy is null, just get a random key (you can use the PRF). */


		if (entropy == NULL){


			unsigned char* buffOne = (unsigned char*)malloc(HM_LEN);
			unsigned char* buffTwo = (unsigned char*) malloc(HM_LEN);


			//WE generate a random aesKEy and hmacKEy
			//buffOne corresponds to AESKey and buffTwo we use with the HMACKey
			randBytes(buffOne,HM_LEN);
			randBytes(buffTwo,HM_LEN);


        //Ptting the values of buffONe and buffTwo in their respective keys
			memcpy((*K).aesKey,buffOne,HM_LEN);
			memcpy((*K).hmacKey, buffTwo,HM_LEN);


		}else{
			unsigned char* keys = (unsigned char*) malloc(HM_LEN * 2);


			//Else we get a 512 bit authentication code with the KDF_KEY
			HMAC(EVP_sha512(), &KDF_KEY,HM_LEN,entropy,entLen,keys,NULL);

			memcpy((*K).aesKey,keys,HM_LEN);
			memcpy((*K).hmacKey, keys+HM_LEN,HM_LEN);
			free(keys);

		}


	return 0;
}
size_t ske_getOutputLen(size_t inputLen)
{
	return AES_BLOCK_SIZE + inputLen + HM_LEN;
}
size_t ske_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K, unsigned char* IV)
{


  //Here outbuf is CT, then inBuf is the message
  int number_written = 0;

  //If there is no IV passed, we generate a randon IV of size 16
  if (!IV){
    IV=malloc(HM_LEN/2);
    }

  // We now start encrypting with our OpenSSL function
    EVP_CIPHER_CTX* cipher_text = EVP_CIPHER_CTX_new();
    if ( EVP_EncryptInit_ex(cipher_text,EVP_aes_256_ctr(),0,K->aesKey,IV) == 0){

      printf("Error");
      }
      unsigned char* outBuf_ct = malloc (len);
      if (EVP_EncryptUpdate(cipher_text,outBuf_ct,&number_written,inBuf,len) == 0){

       printf("Error");
      }

      // Here we are going to compute the hmac of IV plus CT
      unsigned char* iv_plus_ct =  (unsigned char*) malloc(16+number_written);

        //We will now get together the message

      unsigned char* iv_plus_ct_hmac = (unsigned char* )malloc((HM_LEN/2) + number_written + HM_LEN);



      unsigned char* md_result = ( unsigned char*)malloc(HM_LEN);



     //Copy the data from IV type casted to iv_plus with a size of 16
      memcpy(iv_plus_ct,IV,(HM_LEN/2));

      //Size of len
      memcpy(iv_plus_ct+(HM_LEN/2),outBuf_ct,number_written);


      //This has the value of hmac of ct
      HMAC(EVP_sha256(),K->hmacKey,HM_LEN,iv_plus_ct, (HM_LEN/2)+ number_written,md_result, NULL);
      memcpy(iv_plus_ct_hmac, iv_plus_ct, (HM_LEN/2) + number_written);
      memcpy(iv_plus_ct_hmac+ (HM_LEN/2) + number_written, md_result,HM_LEN);

      //Putting the value of iv_ct_hmac to outbuf
      memcpy(outBuf, iv_plus_ct_hmac, (HM_LEN/2) + number_written+HM_LEN);

      //Returns the number of bytes written
      return (16 +number_written+ HM_LEN);
}


size_t ske_encrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, unsigned char* IV, size_t offset_out)
{
	/* TODO: write this.  Hint: mmap. */


  int fdin  = open(fnin, O_RDONLY);
	int fdout = open(fnout, O_CREAT | O_RDWR, S_IRWXU);
  size_t fileSize ;
  if ((fdin < 1) || (fdout < 1)){

    printf("Files did not open");
    return -1;

  }

	struct stat statusBuffer;

  //Getting the file size
  stat(fnin,&statusBuffer);
  fileSize = statusBuffer.st_size;

	if(fstat(fdin, &statusBuffer) <1  || fileSize<1 ) {

    return -1;
    }

   //Creating mapping of file and memory
	char *mapping = mmap(NULL, fileSize, PROT_READ, MAP_PRIVATE, fdin, 0);
	unsigned char* ciphertext = malloc(ske_getOutputLen(strlen(mapping) + 1)+1);


	if(IV == NULL) {
		IV = malloc(16);
		randBytes(IV, 16);

	}

  //Encrypting
	ske_encrypt(ciphertext, (unsigned char*)mapping, fileSize, K, IV);


  //Repositions the offset to write
	lseek(fdout, offset_out, SEEK_SET);
	write(fdout, ciphertext, ske_encrypt(ciphertext, (unsigned char*)mapping, fileSize, K, IV));

 //Removing mapping  from mapping continuining for fileSize bytes
	munmap(mapping, fileSize);

	close(fdin);
	close(fdout);
	return 0;

}
size_t ske_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K)
{
	/* TODO: write this.  Make sure you check the mac before decypting!
	 * Oh, and also, return -1 if the ciphertext is found invalid.
	 * Otherwise, return the number of bytes written.  See aes-example.c
	 * for how to do basic decryption. */

    int numberWritten=0;
    unsigned char* newmac = malloc(HM_LEN);
    HMAC(EVP_sha256(),(*K).hmacKey,HM_LEN,inBuf,len-HM_LEN,newmac,NULL);

     if (memcmp(newmac,inBuf+len-HM_LEN,HM_LEN) !=0 ){

    return -1;

    }
     // Copy inBuf to IV with a size of 16 bytes
    unsigned char* IV =  (unsigned char*) malloc((HM_LEN/2));
    memcpy(IV,inBuf,(HM_LEN/2));

    //Building our ciphertext for decryption
    EVP_CIPHER_CTX* cipher_text = EVP_CIPHER_CTX_new();

    if (EVP_DecryptInit_ex(cipher_text,EVP_aes_256_ctr(),NULL,(*K).aesKey,IV) != 1){
      printf("Error");
    }


    if(EVP_DecryptUpdate(cipher_text,outBuf,&numberWritten, inBuf + (HM_LEN/2), len- (HM_LEN/2) - HM_LEN) != 1){
     perror("Error");

      }
		return 0;
}


size_t ske_decrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, size_t offset_in)
{

  //Creating this variable to keep track of the into about our file
    struct stat statusBuffer;


    int fdin  = open(fnin, O_RDONLY);
    int fdout = open(fnout, O_CREAT | O_RDWR, S_IRWXU);

  //If there is an error opening the files, return -1
    if(fdin <1 || fdout <1)
      {
        return -1;
      }

  // If the information about the open file written on statusBuffer is less than 1 or the length of it is 0, then we get an error
	if(fstat(fdin, &statusBuffer) < 1|| statusBuffer.st_size == 0) {

    return -1;

    }


 //Creating mapping of file and memory
	unsigned char *mapping;
	mapping = mmap(NULL, statusBuffer.st_size, PROT_READ, MAP_PRIVATE, fdin, offset_in);


  //Decrypting our ciphertext
	char* decryptedText = malloc(statusBuffer.st_size-16-HM_LEN-offset_in);
	ske_decrypt((unsigned char*)decryptedText, mapping, statusBuffer.st_size-offset_in, K);


    //Opening our file with write permits
    FILE *outputFile = fopen(fnout, "w");

      //Writing our decrypted text to the file
		fputs(decryptedText, outputFile);
		fclose(outputFile);
    return 0;
}
